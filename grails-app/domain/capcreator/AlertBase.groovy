package capcreator

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

@Slf4j
class AlertBase implements MultiTenant<AlertBase>, ACLControlled {

  String id
  String title

  Party owner

  static mapping = {
            id column: 'ab_id', generator: 'uuid2', length:36
         title column: 'ab_title'
         owner column: 'ab_owner_party_fk'
  }

  static constraints = {
    title nullable:false, blank:false
    owner nullable:false
  }

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findAlertBaseByLuceneQuery':[ methodName:'internalFindAllByLuceneQueryString', 
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findAlertBaseByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

  public static PagedResultList internalFindAllByLuceneQueryString(String luceneQueryString, Map context, Map metaParams=[:]) {
    log.debug("internalFindAllByLuceneQueryString(${luceneQueryString},${context},${metaParams})");
    grails.orm.PagedResultList result = null;

    // Creare a criteria query from the lucene query string that honours the security grants in place
    try {
      log.debug("Current user: ${context?.springSecurityService?.currentUser}");

      String username = context?.springSecurityService?.principal?.username

      if ( username ) {
        // Build criteria for the actual user query
        // whereLuceneQueryString is added to domain classes by the gormwherelucenequery plugin
        DetachedCriteria query_dq = whereLuceneQueryString(luceneQueryString)
        DetachedCriteria valid_grant_subquery = getGrantSubquery ('READ',
                                                       username,
                                                       context?.springSecurityService?.principal?.authorities,
                                                       AlertBase.class.name,
                                                       'ab_id',  // The SQL column that BaseAlert.id gets mapped to - for explicit grants against a specific alert
                                                       'owner'); // If there is an owning "party" context put the property name here - the "Party owner" prop above
                                                                 // For grants made to the owning "context" of a resource
  

        result = this.createCriteria().list(metaParams) {

          // Create the context that will allow the valid_grant_query to join the owner groups
          createAlias('owner', 'ownerpty');

          // Our actual query critera
          addToCriteria(Subqueries.propertyIn( 'id', query_dq.setProjection(Projections.distinct(Projections.id()))))

          // Only return rows where there is a valid grant that enables the user to see the row
          addToCriteria(Subqueries.exists(valid_grant_subquery.setProjection(Projections.property("grant.id"))))
        };
      }

    }
    catch ( Exception e ) {
      e.printStackTrace()
    }

    // return findAllByLuceneQueryString(luceneQueryString, metaParams)
    // log.debug("Secured query result: ${result}");
    return result;
  }

}
