package capcreator

import grails.gorm.annotation.Entity
import grails.gorm.MultiTenant

class Widget implements MultiTenant<Widget> {

  String id
  String widgetName

  static query_config = [
    defaultField:'widgetName',
    properties:[
      widgetName:[mode:'keyword']
    ]
  ]

  static constraints = {
    widgetName(nullable:false, blank:false)
  }

  static mapping = {
            id column:'w_id', generator: 'uuid2', length:36
    widgetName column: 'w_name'
  }

}
