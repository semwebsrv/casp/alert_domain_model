import groovy.sql.Sql

databaseChangeLog = {

  // Define the migrations for your application here:
  changeSet(author: "ianibbo (manual)", id: "cap-creator-001-1") {
    createTable(tableName: "alert_base") {
      column(name: "ab_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "alertBasePk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "ab_title",          type: "VARCHAR(256)")     { constraints(nullable: "false") }
      column(name: "ab_owner_party_fk", type: "VARCHAR(36)")      { constraints(nullable: "false") }
    }

    createTable(tableName: "widget") {
      column(name: "w_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "widgetPk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "w_name",            type: "VARCHAR(256)")     { constraints(nullable: "false") }
    }

    grailsChange {
      change {
        // grailsChange gives us an sql variable which inherits the current connection, and hence should
        // get the schema
        // sql.execute seems to get a bit confused when passed a GString. Work it out before
        def cmd = "CREATE INDEX alert_title_gin_idx ON ${database.defaultSchemaName}.alert_base USING GIN (ab_title gin_trgm_ops)".toString()
        sql.execute(cmd);

        confirm 'Gin index on alert_base(ab_title) created'
      }
    }
  }

}
